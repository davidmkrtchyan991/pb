<?php

namespace App\Utils;

use App\Classes\enums\ChecklistItemStatusEnum;
use App\Classes\enums\OrderOperationsEnum;

abstract class ChecklistItemUtils
{
    public static function getStatusesConfig($item)
    {
        $user = AppUtils::getUser();
        $defaultConf = ['view' => $item->status];
        if ($user->isOptimizer()) {
            switch ($item->status) {
                case ChecklistItemStatusEnum::ASSIGNED:
                    $defaultConf = ['operations' => [ChecklistItemStatusEnum::ASSIGNED, ChecklistItemStatusEnum::DONE], 'view' => $item->status];
                    break;
                default:
                    $defaultConf = ['view' => $item->status];
            }
        } else if ($user->isTechManager()) {
            switch ($item->status) {
                case ChecklistItemStatusEnum::WAITING_TO_BE_ASSIGNED:
                    $defaultConf = [
                        'operations' => [ChecklistItemStatusEnum::WAITING_TO_BE_ASSIGNED, ChecklistItemStatusEnum::ASSIGNED],
                        'view' => $item->status
                    ];
                    break;
                case ChecklistItemStatusEnum::ASSIGNED:
                    $defaultConf = [
                        'operations' => [ChecklistItemStatusEnum::ASSIGNED, ChecklistItemStatusEnum::WAITING_TO_BE_ASSIGNED],
                        'view' => $item->status
                    ];
                    break;
                default:
                    $defaultConf = ['view' => $item->status];
            }
        }
        return collect($defaultConf);
    }

    public static function isItemEditable($order, $item)
    {
        $isItemEditableOperation = collect([OrderOperationsEnum::ASSIGN_TO_OPTIMIZER, OrderOperationsEnum::UPDATE_CHECKLIST_STATUS])->contains($order->commitOperation);
        return self::getStatusesConfig($item)->get('operations') && $isItemEditableOperation;
    }

    public static function getItemsPercentagesGroup($order)
    {
        $allItems = collect([]);
        $order->checklists->each(function ($orderChecklist, $i) use ($allItems) {
            $orderChecklist->items->each(function ($orderChecklistItem, $i1) use ($allItems) {
                $allItems->push($orderChecklistItem);
            });
        });

        $groupedCounts = $allItems->groupBy("status")->map(function ($items, $status) {
            return collect($items)->count();
        });

        return collect(['groupedCounts' => $groupedCounts, 'totalCount' => $allItems->count()]);
    }

    public static function getPercentageForStatus($itemsPercentagesGroup, $status)
    {
        $totalCount = collect($itemsPercentagesGroup)->get('totalCount');
        $statusCount = collect($itemsPercentagesGroup)->get('groupedCounts')->get($status);

        return ($totalCount > 0) ? round((100 * $statusCount) / $totalCount, 2) : 0;
    }

    public static function getAllItemsForOrder($order)
    {
        $allItems = collect([]);
        $order->checklists->each(function ($orderChecklist, $i) use ($allItems) {
            $orderChecklist->items->each(function ($orderChecklistItem, $i1) use ($allItems) {
                $allItems->push($orderChecklistItem);
            });
        });
        return $allItems;
    }
}