<?php

namespace App\Utils;

use App\Classes\enums\OrderOperationsEnum;
use App\Classes\enums\OrderStatusEnum;
use App\Classes\enums\RoleEnum;
use App\Order;
use Illuminate\Support\Facades\Lang;

abstract class OrderUtils
{
    const OPERATIONS_CONFIGURATION = [
        null => [
            OrderOperationsEnum::REGISTER => [
                'actionToPersist' => 'store',
                'roles' => [RoleEnum::ROLE_ADMINISTRATOR],
            ]
        ],
        OrderStatusEnum::REGISTERED => [
            OrderOperationsEnum::UPDATE_REGISTERED => [
                'actionToGet' => 'edit',
                'actionToPersist' => 'update',
                'roles' => [RoleEnum::ROLE_ADMINISTRATOR],
            ],
            OrderOperationsEnum::ASSIGN_TO_TECH_MANAGER => [
                'actionToGet' => 'getToAssignTechManager',
                'actionToPersist' => 'assignTechManager',
                'roles' => [RoleEnum::ROLE_ADMINISTRATOR],
            ],
        ],
        OrderStatusEnum::ASSIGNED_TO_TECH_MANAGER => [
            OrderOperationsEnum::ASSIGN_TO_OPTIMIZER => [
                'actionToGet' => 'getToAssignChecklist',
                'actionToPersist' => 'assignChecklist',
                'roles' => [RoleEnum::ROLE_TECHNICAL_MANAGER],
            ],
            OrderOperationsEnum::ADD_EXCEPTIONAL_CHECKLIST => [
                'actionToGet' => 'getToAddExceptionalChecklist',
                'actionToPersist' => 'addExceptionalChecklist',
                'roles' => [RoleEnum::ROLE_TECHNICAL_MANAGER],
            ],
        ],
        OrderStatusEnum::ASSIGNED_TO_OPTIMIZER => [
            OrderOperationsEnum::ADD_EXCEPTIONAL_CHECKLIST => [
                'actionToGet' => 'getToAddExceptionalChecklist',
                'actionToPersist' => 'addExceptionalChecklist',
                'roles' => [RoleEnum::ROLE_TECHNICAL_MANAGER],
            ],
            OrderOperationsEnum::ASSIGN_TO_OPTIMIZER => [
                'actionToGet' => 'getToAssignChecklist',
                'actionToPersist' => 'assignChecklist',
                'roles' => [RoleEnum::ROLE_TECHNICAL_MANAGER],
            ],
            OrderOperationsEnum::COMPLETE => [
                'actionToGet' => 'getToSetCompleted',
                'actionToPersist' => 'complete',
                'roles' => [RoleEnum::ROLE_TECHNICAL_MANAGER],
            ],
            OrderOperationsEnum::UPDATE_CHECKLIST_STATUS => [
                'actionToGet' => 'getToUpdateChecklistStatus',
                'actionToPersist' => 'updateChecklistStatus',
                'roles' => [RoleEnum::ROLE_OPTIMIZER],
            ],
            OrderOperationsEnum::UPDATE_CLIENT_KEYWORDS => [
                'actionToGet' => 'getToUpdateClientKeywords',
                'actionToPersist' => 'updateClientKeywords',
                'roles' => [RoleEnum::ROLE_CLIENT],
            ],
            OrderOperationsEnum::CONFIRM_CLIENT_KEYWORDS => [
                'actionToGet' => 'getToConfirmClientKeywords',
                'actionToPersist' => 'confirmClientKeywords',
                'roles' => [RoleEnum::ROLE_TECHNICAL_MANAGER],
            ],
        ],
    ];

    public static function initOperations(Order $order)
    {
        $order->operations = self::getOperationsConfigurationForStatus($order->status);
    }

    public static function getActionToPersist($status, $operation)
    {
        return collect(self::getOperationsConfigurationForStatus($status)->get($operation))->get('actionToPersist');
    }

    public static function getOperationsConfigurationForStatus($status)
    {
        return collect(collect(self::OPERATIONS_CONFIGURATION)->get($status));
    }


    /*Other*/

    public static function isOptimizerTabVisible($order)
    {
        return ($order->isAssignChecklist || $order->optimizer) && !(AppUtils::getUser()->isClient());
    }

    public static function isChecklistAssignable(Order $order)
    {
        return AppUtils::getUser()->isTechManager() && $order->isAssignChecklist;
    }

    public static function userHasAccessToOrder($order, $user = null)
    {
        $user = $user ?: AppUtils::getUser();
        if ($order && $user) {
            if ($user->isAdministrator() || $user->isTechManager()) {
                return true;
            } else if ($user->isOptimizer()) {
                return $order->optimizer && $order->optimizer->id == $user->id;
            } else if ($user->isClient()) {
                return $order->user && $order->user->id == $user->id;
            }
        }
        return false;
    }

    public static function getOrderKeywordsCount($order)
    {
        return self::getKeywordsCountInString($order->keywords);
    }

    public static function getKeywordsCountInString($keywords)
    {
        return $keywords ? sizeof(explode(',', $keywords)) : 0;
    }


    public static function getOrdersPercentagesGroup($user = null)
    {
        $user = $user ?: AppUtils::getUser();
        $userOrders = Order::withUser($user->id)->get();

        $groupedCounts = $userOrders->groupBy("status")->map(function ($orders, $status) {
            return collect($orders)->count();
        });

        return collect(['groupedCounts' => $groupedCounts, 'totalCount' => $userOrders->count()]);
    }

    public static function getPercentageForStatus($ordersPercentagesGroup, $status)
    {
        $totalCount = collect($ordersPercentagesGroup)->get('totalCount');
        $statusCount = collect($ordersPercentagesGroup)->get('groupedCounts')->get($status);

        return ($totalCount > 0) ? round((100 * $statusCount) / $totalCount, 2) : 0;
    }

    public static function getOrderSavedSuccessMessage(){
        return Lang::get('order.saved.message');
    }

    public static function getOrderUpdatedSuccessMessage(){
        return Lang::get('order.updated.message');
    }
}