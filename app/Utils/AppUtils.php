<?php

namespace App\Utils;

use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

abstract class AppUtils
{
    const DATE_FORMAT = "d/m/Y";

    public static function convertStringToDate($date)
    {
        return Carbon::createFromFormat(self::DATE_FORMAT, $date);
    }

    public static function getUser()
    {
        return Auth::user();
    }

    public static function buildMapByDelimiter($array, $delimiter = '-')
    {
        $statusesMapping = collect([]);
        collect($array)->each(function ($idsMapping, $i) use ($statusesMapping, $delimiter) {
            $key = explode($delimiter, $idsMapping)[0];
            $value = explode($delimiter, $idsMapping)[1];
            $statusesMapping->put($key, $value);
        });
        return $statusesMapping;
    }
}